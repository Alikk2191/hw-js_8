// Опишіть своїми словами що таке Document Object Model (DOM)
    // Це звʼязуючий інтерфес між скріптом та сторінкою WEB. За допомогою DOM JS може створювати динамічні єлеименти або модифікувати сторінку з елементами

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    // За допомогою innerHTML можна вставляти код HTML зі всими його властивостями( стилі, сласи, id  та інше). За допомогою innerText можна вставити лише текст

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
  // getElementById, getElementsByClassName, getElementsByTagName, querySelector, querySelectorAll, все залижить від мети пошуку немає кращого способу


const findParagraphs = document.getElementsByTagName("p")
for (const element of findParagraphs) {
  element.style.backgroundColor = "#ff0000"; 
}

const optionsList = document.getElementById("optionsList")
console.log(optionsList);

const parentElement = optionsList.parentNode;
console.log(parentElement);

var childNodes = optionsList.childNodes;
for (const childNode of childNodes) {
    console.log('Назва: ' + childNode.nodeName + ', Тип: ' + childNode.nodeType);
}

const testParagraph = document.getElementsByClassName("testParagraph")
testParagraph.textContent = " This is a paragraph";


const mainHeader = document.getElementsByClassName('main-header')[0];
const innerElements = mainHeader.getElementsByTagName('*');
for (let i = 0; i < innerElements.length; i++) {
    const element = innerElements[i];
    console.log(element);
    element.classList.add('nav-item');
}

const sectionTitles = document.getElementsByClassName('section-title');
for (const element of sectionTitles) {
  element.classList.remove('section-title');
}
console.log(sectionTitles);